FROM openjdk:8-alpine
ADD build/libs/bookshop-0.0.1-SNAPSHOT.jar app.jar
ENV JAVA_OPTS="-Xms256M -Xmx512M -Xss512k"
ENTRYPOINT ["java", "-jar","app.jar"]